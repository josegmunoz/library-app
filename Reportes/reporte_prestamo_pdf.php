<?php
require "fpdf.php";

$servidor="localhost";
$usuario="root";
$password="";
$db_name="libreria";

$connection=mysqli_connect($servidor,$usuario,$password,$db_name);

class myPDF extends FPDF{
    
    function header(){
        $this->SetFont('Arial','B',14);
        $this->Cell(0,5,'Reporte de prestamos',0,0,'C');
        $this->Ln(20);
    }
    
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',14);
        $this->Cell(500,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function headerTable(){
        $this->SetFont('Arial','B',10);
        $this->Cell(25,10,'ID',1,0,'C');
        $this->Cell(50,10,'Titulo',1,0,'C');
        $this->Cell(35,10,'Usuario',1,0,'C');
        $this->Cell(25,10,'Fecha Ini.',1,0,'C');
        $this->Cell(25,10,'Fecha Fin.',1,0,'C');
        $this->Cell(23,10,'Multa',1,0,'C');
        $this->Cell(35,10,'Dias Res.',1,0,'C');
        $this->Cell(30,10,'Estado',1,0,'C');
        $this->Ln();
    }
    
    function viewTable($connection){
        $this->SetFont('Arial','',8);   
        session_start();
        $query = $_SESSION["pre_res"];
        $result=mysqli_query($connection,$query);
        while($rows=mysqli_fetch_assoc($result)){
            $titleid = $rows["id_libro"];
            $userid = $rows["id_usuario"];
            $title_res = mysqli_query($connection,"SELECT * FROM libro WHERE id = '$titleid'");
            $user_res = mysqli_query ($connection,"SELECT * FROM usuario WHERE id = '$userid'");
            $title_rows = mysqli_fetch_assoc($title_res);
            $user_rows = mysqli_fetch_assoc($user_res);
            $this->Cell(25,10,$rows["id"],1,0,'L');
            $this->Cell(50,10,$title_rows["titulo"],1,0,'L');
            $this->Cell(35,10,$user_rows["usuario"],1,0,'L');
            $this->Cell(25,10,$rows["fecha_ini"],1,0,'L');
            $this->Cell(25,10,$rows["fecha_fin"],1,0,'L');
            $this->Cell(23,10,$rows["multa"],1,0,'L');
            $this->Cell(35,10,$rows["dias_res"],1,0,'L');
            $this->Cell(30,10,$rows["estado"],1,0,'L');
            $this->Ln();
        }
    }
}

$pdf=new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable($connection);
$pdf->Output();

?>