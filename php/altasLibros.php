<?php

$servidor="localhost";
$usuario="root";
$password="";
$db_name="libreria";

$id_libro="";
$asig_ID="";
$titulo=$_POST["titulo"];
$editorial=$_POST["editorial"];
$edicion=$_POST["edicion"];
$volumen=$_POST["volumen"];
$autor=$_POST["autor"];
$idUb="";
$asignatura=$_POST["asignatura"];
$estado="disponible";


//Este switch maneja el codigo alfabetico de 3 caracteres para el id del libro
//Si se agrego una asignatura en el html, se tiene que agregar aqui tambien
//probando para ver si usamos idUb como algo fijo y unico segun asignatura

switch($asignatura){
    case "Psicologia":
        $asig_ID="PSI";
        $idUb=101;
        break;
    case "Biologia":
        $asig_ID="BIO";
        $idUb=102;
        break;
    case "Matematicas":
        $asig_ID="MAT";
        $idUb=103;
        break;
    case "Computacion":
        $asig_ID="COM";
        $idUb=104;
        break;
    case "Ingenieria":
        $asig_ID="ING";
        $idUb=105;
        break;
}

$connection=mysqli_connect($servidor,$usuario,$password,$db_name);
if(!$connection)
{
    echo "Error: No se puede conectar a la base de datos MySQL".PHP_EOL."<br>";
    echo "Error de depuracion ".mysqli_connect_errno().PHP_EOL."<br>";
    echo "Error de depuracion ".mysqli_connect_errno().PHP_EOL."<br>";
    exit;
}else
{
    echo "Se realizo la conexion de la base de datos correctamente";
    
    $query_count = "SELECT COUNT(*) as number FROM libro";
    $result_count=mysqli_query($connection,$query_count);
    
    $count=mysqli_fetch_assoc($result_count);
    
    $id_libro=$asig_ID.$idUb.($count['number']+1);
    
    $query_ins = "INSERT into libro(id,titulo,editorial,edicion,autores,id_ubicacion,volumen,asignatura,estado) 
    VALUES ('$id_libro','$titulo','$editorial','$edicion','$autor','$idUb','$volumen','$asignatura','$estado')";
    
    mysqli_query($connection,$query_ins); 
    echo '<script language="javascript">alert("Libro  dado de alto con exito");window.location.href="../menu_admin.html"</script>';
    //header("location: http://localhost/library-app/menu_admin.html");
}
?>