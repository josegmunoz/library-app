<?php
$server = "localhost";
$user_db = "root";
$password_db = "";
$db = "libreria";
$sqlcon = mysqli_connect($server, $user_db, $password_db, $db);

$assign = $_POST["asignatura"] ?? '';

if ($assign == null) {
    $query = "SELECT * FROM libro ";
    session_start();
    $_SESSION['lib_res'] = $query;
} else {
    $assign = $_POST["asignatura"];
    $query = "SELECT * FROM libro WHERE asignatura = '$assign'";
    session_start();
    $_SESSION['lib_res'] = $query;
}
$result = mysqli_query($sqlcon, $query);

?>

<!DOCTYPE html>
<html>

<head>
    <title>Libros</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="forms.css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-8 d-flex justify-content-center">Libros Por Categoria</h1>
            <hr class="my-8">
            <div class="row">
                <div class="col-12"></div>
                <div class="col-12">
                    <form action="libros_categoria_html.php" method="post" class="libraryForm">
                        <div class="formAltas">
                            <h2 class="display-3">Categoria</h2>
                            <div class="input-group mb-8">
                                <select id="asignatura" aria-label="Asignatura" class="form-control" placeholder="Asignatura" name="asignatura" required>
                                    <option value=" "></option>
                                    <option value="Psicologia">Psicologia</option>
                                    <option value="Biologia">Biologia</option>
                                    <option value="Matematicas">Matematicas</option>
                                    <option value="Computacion">Computacion</option>
                                    <option value="Ingenieria">Ingenieria</option>
                                </select>
                                <input type="submit" value="Enter" class="btn btn-primary"><br>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>

        <h2 class="title" class="display-3">Listado de libros </h2>
    </div>


    <?php
    if (mysqli_num_rows($result) > 0) {
    ?>
        <div class="container">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td scope="col">Id</td>
                        <td scope="col">Titulo</td>
                        <td scope="col">Editorial</td>
                        <td scope="col">Edicion</td>
                        <td scope="col">Autores</td>
                        <td scope="col">Id de ubicacion</td>
                        <td scope="col">Volumen</td>
                        <td scope="col">Asignatura</td>
                        <td scope="col">Estado</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    while ($row = mysqli_fetch_array($result)) {
                    ?>
                        <tr>
                            <th score="row" class="tdata"><?php echo $row["id"]; ?></th>
                            <td class="tdata"><?php echo $row["titulo"]; ?></td>
                            <td class="tdata"><?php echo $row["editorial"]; ?></td>
                            <td class="tdata"><?php echo $row["edicion"]; ?></td>
                            <td class="tdata"><?php echo $row["autores"]; ?></td>
                            <td class="tdata"><?php echo $row["id_ubicacion"]; ?></td>
                            <td class="tdata"><?php echo $row["volumen"]; ?></td>
                            <td class="tdata"><?php echo $row["asignatura"]; ?></td>
                            <td class="tdata"><?php echo $row["estado"]; ?></td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    <?php
    } else {
        echo "<h2 class=\"text-center\"> No se encontraron libros </h2>";
    }
    ?>
    <div class="container">
        <div class="col"></div>
        <div class="col">
            <div class="col">

                <br><a role="button" class="btn btn-lg btn-block btn-danger" href="/library-app/menu_admin.html">Salir</a>
                <br>
                <br>
            </div>
        </div>
        <div class="col"></div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>