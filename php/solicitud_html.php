<?php
$servidor = "localhost";
$usuario = "root";
$password = "";
$db_name = "libreria";
$connection = mysqli_connect($servidor, $usuario, $password, $db_name);


session_start();
$user = $_SESSION["user_b"];
$result1 = mysqli_query($connection, "SELECT * FROM usuario WHERE usuario = '$user' AND solicitud > 0");
$rownum1 = mysqli_num_rows($result1);

if ($rownum1 > 0 ) {
    echo '<script language="javascript">alert("Ya tienes una transaccion en curso");window.location.href="../menu_usuario.html"</script>';
   // echo '<script language="javascript">alert("Libro apartado con exito");window.location.href="../menu_usuario.html"</script>';
} else {
    //echo '<script language="javascript">alert("Ya tienes una transaccion en curso");window.location.href="../menu_usuario.html"</script>';
    
}
?>

<!doctype HTML>

<html>

<head>
    <title>Solicitud Biblioteca UAA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btn_ocultar').hide();
        });
    </script>

</head>

<body>
    <div class="container">
       <div class="jumbotron">
            <h1 class="display-4 d-flex justify-content-center">Solicitar </h1>
            <hr class="my-4">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <form action="solicitud_usuario.php" method="post" class="libraryForm">
                        <div class="formAltas">
                        <ul>
                            <li>
                            <label for="id_libro">ID del libro</label>
                            <input type="text" id="idU" name="idU" placeholder="ID del libro">
                        </li>
                        </ul>
                            <div class="input-group mb-5 d-flex justify-content-center">
                                <button class="btn btn-primary" class="button" type="submit" value="alta">Solicitud</button>
                                <button class="btn btn-primary" class="button" type="reset" value="regresar">Borrar Datos</button>
                                <input class="btn btn-warning" type="button" onclick="cargarJson()" value="Ver Libros disponibles" />
                                <input class="btn btn-secondary" type="button" onclick="ocultar()" id="btn_ocultar"
                                value="Ocultar" />
                                <button class="btn btn-dark" ><a href="/library-app/menu_usuario.html" class="option">Regresar</a></button>
                            </div>
                            <div id="mostrar"></div>
                        </div>
                    </form>
                     </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

<script>
    function cargarJson() {
        const request = new XMLHttpRequest();
        request.open('GET', 'domsolicitud.php');
        request.onload = () => {
            try {
                var json = JSON.parse(request.responseText);

                Tabla(json);
            } catch (e) {
                alert("porque");
                console.log("EXISTE ERROR DE TIPO: " + request.status);
            }
        }
        request.send();
    }

    function Tabla(json) {
        var result = [];
        json.forEach(element => {
            var set = Object.values(element);
            result.push(set);
        });

        var tabladinamica = document.createElement('table');//Se crea una tabla 
        tabladinamica.setAttribute('id', 'users-table');                     //Se le asigna un id
        tabladinamica.setAttribute('class', 'table table-striped table-primary');   //Se le asigna el formato de bootstrap
        var thead = document.createElement('thead');// Se genra un thead
        thead.setAttribute('class', 'thead-dark');//Se le asigna estilo
        tabladinamica.appendChild(thead);
        var tr = tabladinamica.insertRow(0);
        var th_id = document.createElement('th');  //Se crean las columnas de la tabla
        var th_libro = document.createElement('th');
        var th_asig = document.createElement('th');
        var th_estado = document.createElement('th');


        th_id.innerHTML = "ID ";                 //Se les da un nombre
        th_libro.innerHTML = "Libro";
        th_asig.innerHTML = "Asignatura";
        th_estado.innerHTML = "Estado";

        tr.appendChild(th_id);    //Se abren tr apartir de los thead (padre->hijo) 
        tr.appendChild(th_libro);
        tr.appendChild(th_asig);
        tr.appendChild(th_estado);


        thead.appendChild(tr)
        result.forEach((row) => {                       //Ciclos para llenar la tabla
            const tr_new = document.createElement("tr");
            row.forEach((cell) => {
                const td_new = document.createElement("td");
                td_new.textContent = cell;
                tr_new.appendChild(td_new);

                console.log(cell);
            })
            tabladinamica.appendChild(tr_new);
        });
        $('#btn_ocultar').show();
        var divContainer = document.getElementById("mostrar");
        divContainer.setAttribute("class", "container")
        divContainer.innerHTML = "";
        divContainer.appendChild(tabladinamica);
        $('#mostrar').show();
    }
    function ocultar() {
        $('#mostrar').hide();
        $('#btn_ocultar').hide();
    }
</script>

</html>